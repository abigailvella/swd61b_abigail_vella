package com.example.polarbear;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SearchCountry extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_country);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvFavs);
        LinearLayoutManager llm = new LinearLayoutManager(getBaseContext());
        rv.setLayoutManager(llm);

        FloatingActionButton fabFavs = findViewById(R.id.fabFavourites);
        fabFavs.hide();

        BottomNavigationView navigation = findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_favourites:
                        Intent intent = new Intent(SearchCountry.this, Favourites.class);
                        startActivity(intent);
                        return true;
                    case R.id.navigation_home:
                        Intent intent2 = new Intent(SearchCountry.this, MainNavigation.class);
                        startActivity(intent2);
                        return true;
                }
                return false;
            }
        });

        fabFavs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                /*Favourites favourites = new Favourites();
                favourites.GetCountry(v);
                Toast.makeText(getApplicationContext(), "Added to Favourites", Toast.LENGTH_LONG).show();*/
            }
        });

    }

    public void getCountry(View v){

        EditText etxtSearch = findViewById(R.id.txtCountry);
        GetCountry task = new GetCountry();
        RecyclerView rv = (RecyclerView)findViewById(R.id.rvFavs);


        try {
            List<Country> countries = task.execute(etxtSearch.getText().toString()).get();

            if(countries == null) {
                Toast.makeText(this, "Country not found!", Toast.LENGTH_LONG).show();
                FloatingActionButton fabFavs = findViewById(R.id.fabFavourites);
                fabFavs.hide();
            }

            RVAdapter adapter = new RVAdapter(countries);
            rv.setAdapter(adapter);
            FloatingActionButton fabFavs = findViewById(R.id.fabFavourites);
            fabFavs.show();

            //Toast.makeText(getApplicationContext(), "Added to Favourites", Toast.LENGTH_LONG).show();

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Toast.makeText(getApplicationContext(), "onStart called", Toast.LENGTH_LONG).show();
        CoordinatorLayout coordinatorLayout = findViewById(R.id.coordinatorLayout);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, Calendar.getInstance().getTime().toString(), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(getApplicationContext(), "onResumed called", Toast.LENGTH_LONG).show();
        CoordinatorLayout coordinatorLayout = findViewById(R.id.coordinatorLayout);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, Calendar.getInstance().getTime().toString(), Snackbar.LENGTH_LONG);
        snackbar.show();
    }
    @Override
    protected void onPause() {
        super.onPause();
        EditText txtCountry = findViewById(R.id.txtCountry);
        Toast.makeText(getApplicationContext(), "OnPause " + txtCountry.getText() , Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onStop() {
        super.onStop();  // Always call the superclass method first
        EditText txtCountry = findViewById(R.id.txtCountry);
        Toast.makeText(getApplicationContext(), "OnStop " + txtCountry.getText() , Toast.LENGTH_LONG).show();
    }
}

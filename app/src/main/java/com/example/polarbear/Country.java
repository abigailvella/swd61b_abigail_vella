package com.example.polarbear;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class Country {

    String countryName;
    String capitalCity;
    String countryCode;
    String region;
    String timeZone;
    String callingCode;
    String population;
    String currency;
    Bitmap flag;

    Country()
    {
        //If flag img does not exist, place default img
        InputStream is= null;
        try {
            is = new URL("https://www.warrenhannonjeweler.com/static/images/temp-inventory-landing.jpg").openStream();
            this.flag = BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Country(String countryName, String capitalCity, String countryCode, String region, String timeZone, String callingCode, String population, String currency, Bitmap flag)
    {
        this.countryName = countryName;
        this.capitalCity = capitalCity;
        this.countryCode = countryCode;
        this.region = region;
        this.timeZone = timeZone;
        this.callingCode = callingCode;
        this.population = population;
        this.currency = currency;
        this.flag = flag;
    }
}

package com.example.polarbear;

import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGImageView;
import com.caverock.androidsvg.SVGParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetCountry extends AsyncTask<String, Void, List<Country>> {
    @Override
    protected List<Country> doInBackground(String... countryName) {
        CountryHttpClient client = new CountryHttpClient();

        String resultJSON = client.GetCountryData(countryName[0]);
        JSONObject jObj = null;
        try {
            List<Country> countries = new ArrayList();
            JSONArray jArr = new JSONArray(resultJSON);

            for(int i = 0; i < jArr.length(); i++){
                Country country = new Country();
                JSONObject jObject = jArr.getJSONObject(0);

                country.countryName = jObject.getString("name");
                country.capitalCity = jObject.getString("capital");
                country.countryCode = jObject.getString("alpha3Code");
                country.region = jObject.getString("region");
                country.timeZone = jObject.getString("timezones");
                country.callingCode = jObject.getString("callingCodes");
                country.currency = jObject.getString("currencies");
                country.population = jObject.getString("population");
                InputStream is = null;
                try {
                    is = new URL(jObject.getString("flag")).openStream();
                    SVG svg = SVG.getFromInputStream(is);
                    //Drawable drawable = Drawable(svg.renderToPicture(is));
                    //SVGImageView imageView = new SVGImageView();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SVGParseException e) {
                    e.printStackTrace();
                }
                //country.flag = BitmapFactory.decodeStream(is);

                countries.add(country);
            }

            return countries;

        } catch (JSONException e) {
            e.printStackTrace();
        } /*catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        return null;
    }
}

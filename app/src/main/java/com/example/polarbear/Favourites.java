package com.example.polarbear;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class Favourites extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favourites);

        BottomNavigationView navigation = findViewById(R.id.navigation);

        navigation.getMenu().findItem(R.id.navigation_favourites).setChecked(true);
        navigation.getMenu().findItem(R.id.navigation_home).setChecked(false);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent intent2 = new Intent(Favourites.this, MainNavigation.class);
                        startActivity(intent2);
                        return true;
                }
                return false;
            }
        });
    }

    public void GetCountry(View v)
    {
        EditText etxtSearch = findViewById(R.id.txtCountry);
        GetCountry task = new GetCountry();
        RecyclerView rv = (RecyclerView)findViewById(R.id.rvFavs);

        try {
            List<Country> countries = task.execute(etxtSearch.getText().toString()).get();

            RVAdapter adapter = new RVAdapter(countries);
            rv.setAdapter(adapter);

            Toast.makeText(getApplicationContext(), "Added to Favourites", Toast.LENGTH_LONG).show();
        } catch (
                ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

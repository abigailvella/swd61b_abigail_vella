package com.example.polarbear;

import android.annotation.TargetApi;
import android.icu.text.UnicodeSet;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConvertHttpClient {
    private static String BASE_URL = "https://currency-exchange.p.rapidapi.com/exchange?q=1.0&from=";
    private static String APP_ID = "835d6eaa69msh832ea2c64ea7ae1p1909b8jsn273ca871f3b9";

    @TargetApi(24)
    public String ConvertCurrency(String currencyFrom, String currencyTo) {
        //HttpURLConnection con = null ;
        OkHttpClient con = new OkHttpClient();
        InputStream is = null;
        try {
            //String url = BASE_URL + currencyFrom.replaceAll(" ", "+");
            String url = BASE_URL + currencyFrom + "&to=" + currencyTo;
            //EUR&to=INR

            Request request = new Request.Builder()
                    .url(url)
                    .header("Connection", "close")
                    .header("X-RapidAPI-Host", "currency-exchange.p.rapidapi.com")
                    .header("X-RapidAPI-Key", "835d6eaa69msh832ea2c64ea7ae1p1909b8jsn273ca871f3b9")
                    .build();

            Response repsonses = null;

            try {
                repsonses = con.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return repsonses.body().string();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            //try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;
    }
}

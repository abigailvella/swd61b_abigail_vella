package com.example.polarbear;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.polarbear.R;

import java.util.ArrayList;
import java.util.List;

public class ConvertCurrency extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.currency_converter);

        Spinner spnFrom = findViewById(R.id.spnFrom);
        Spinner spnTo = findViewById(R.id.spnTo);
        AddItemsSpinner(spnFrom);
        AddItemsSpinner(spnTo);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.getMenu().findItem(R.id.navigation_home).setChecked(false);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_favourites:
                        Intent intent = new Intent(ConvertCurrency.this, Favourites.class);
                        startActivity(intent);
                        return true;
                    case R.id.navigation_home:
                        Intent intent2 = new Intent(ConvertCurrency.this, MainNavigation.class);
                        startActivity(intent2);
                        return true;
                }
                return false;
            }
        });

    }

    public void AddItemsSpinner(Spinner spinner){
        List<String> items = new ArrayList<String>();
        items.add("EUR");
        items.add("USD");
        items.add("GBP");
        items.add("SGD");
        items.add("MYR");
        items.add("JPY");
        items.add("AUD");
        items.add("KRW");
        items.add("CHF");
        items.add("PLN");
        items.add("INR");
        items.add("PHP");
        items.add("THB");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, items);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    public void Convert(View v){

        TextView value = findViewById(R.id.txtAnswer);
        EditText amount = findViewById(R.id.txtAmount);

        Spinner spnFrom = findViewById(R.id.spnFrom);
        Spinner spnTo = findViewById(R.id.spnTo);
        String from = spnFrom.getSelectedItem().toString();
        String to = spnTo.getSelectedItem().toString();

        ConvertHttpClient client = new ConvertHttpClient();
        client.ConvertCurrency(from, to);

        int amt = Integer.parseInt(amount.getText().toString());


        int ans = amt * Integer.parseInt(client.ConvertCurrency(from, to));

        value.setText(amt);

    }
}

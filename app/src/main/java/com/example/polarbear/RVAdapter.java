package com.example.polarbear;
import android.graphics.Movie;
import android.media.Image;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.CountryViewHolder>{

    public static class CountryViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView countryName;
        TextView capitalCity;
        TextView countryCode;
        TextView region;
        TextView timeZone;
        TextView callingCode;
        TextView population;
        ImageView flag;

        CountryViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            countryName = (TextView)itemView.findViewById(R.id.txtCountryName);
            capitalCity = (TextView)itemView.findViewById(R.id.txtCapitalCity);
            countryCode = (TextView)itemView.findViewById(R.id.txtCountryCode);
            region = (TextView)itemView.findViewById(R.id.txtRegion);
            timeZone = (TextView)itemView.findViewById(R.id.txtTimeZone);
            callingCode = (TextView)itemView.findViewById(R.id.txtCallingCode);
            population = (TextView)itemView.findViewById(R.id.txtPopulation);
            flag = (ImageView) itemView.findViewById(R.id.imgFlag);
        }
    }

    List<Country> countries;

    RVAdapter(List<Country> countries){
        this.countries = countries;
    }

    @Override
    public int getItemCount() {
        try
        {
            return countries.size();
        }
        catch (NullPointerException e)
        {
            return 0;
        }
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.liner_layout_country, viewGroup, false);
        CountryViewHolder mvh = new CountryViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(CountryViewHolder CountryViewHolder, int i) {
        CountryViewHolder.countryName.append(countries.get(i).countryName);
        CountryViewHolder.capitalCity.append(countries.get(i).capitalCity);
        CountryViewHolder.countryCode.append(countries.get(i).countryCode);
        CountryViewHolder.region.append(countries.get(i).region);
        CountryViewHolder.timeZone.append(countries.get(i).timeZone);
        CountryViewHolder.callingCode.append(countries.get(i).callingCode);
        CountryViewHolder.population.append(countries.get(i).population);
        CountryViewHolder.flag.setImageBitmap(countries.get(i).flag);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
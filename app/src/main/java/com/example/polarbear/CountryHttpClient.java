package com.example.polarbear;

import android.app.DownloadManager;
import android.graphics.Bitmap;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CountryHttpClient {

    String countryName;
    String capitalCity;
    String countryCode;
    String region;
    String timeZone;
    String callingCode;
    String population;
    //Bitmap flag;

    private static String BASE_URL = "https://restcountries.eu/rest/v2/name/";

    public String GetCountryData(String countryName){
        //HttpURLConnection con = null ;
        OkHttpClient con = new OkHttpClient();
        InputStream is = null;
        try {
            String url = BASE_URL + countryName.replaceAll(" ", "+");

            /*
            con = (HttpURLConnection) ( new URL(url)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();



            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while (  (line = br.readLine()) != null )
                buffer.append(line + "\r\n");

            is.close();
            con.disconnect();
            */

            Request request = new Request.Builder()
                    .url(url)
                    .header("Connection", "close")
                    .build();

            Response repsonses = null;

            try {
                repsonses = con.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return repsonses.body().string();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            //try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;
    }
}
